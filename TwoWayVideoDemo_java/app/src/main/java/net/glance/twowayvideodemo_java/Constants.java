package net.glance.twowayvideodemo_java;

import net.glance.android.VideoMode;

public class Constants {

    public static int GLANCE_GROUP_ID = 21489; // SET YOUR OWN GROUP ID HERE

    public static VideoMode VIDEO_MODE = VideoMode.VideoOff;

    public static String VISITOR_ID = "60twv"; // SET YOUR OWN VISITOR ID FOR PRESENCE HERE
}
