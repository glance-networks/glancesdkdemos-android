package net.glance.twowayvideodemo_kotlin

import net.glance.android.VideoMode

object Constants {

    const val GLANCE_GROUP_ID = 21489 // SET YOUR OWN GROUP ID HERE

    val VIDEO_MODE = VideoMode.VideoSmallMultiway

    const val VISITOR_ID = "60twv" // SET YOUR OWN VISITOR ID FOR PRESENCE HERE
}